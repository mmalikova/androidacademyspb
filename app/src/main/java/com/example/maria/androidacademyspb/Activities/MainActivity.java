package com.example.maria.androidacademyspb.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.maria.androidacademyspb.Models.Speaker;
import com.example.maria.androidacademyspb.R;
import com.example.maria.androidacademyspb.ReporterActivity;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button allTopicsButton = findViewById(R.id.allTopicsButton);
        allTopicsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openTopicsListActivity();
            }
        });

        Button allSpeakersButton = findViewById(R.id.allSpeakersButton);
        allSpeakersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openSpeakersListActivity();
            }
        });
    }

    private void openTopicsListActivity() {
        Intent topicsListActivityIntent = new Intent(this, TopicsListActivity.class);
        startActivity(topicsListActivityIntent);
    }

    private void openSpeakersListActivity() {
        //Intent speakersListActivityIntent = new Intent(this, SpeakersListActivity.class);
        //startActivity(speakersListActivityIntent);

        Intent speakerActivityIntent = new Intent(this, SpeakerActivity.class);
        startActivity(speakerActivityIntent);
    }


}
