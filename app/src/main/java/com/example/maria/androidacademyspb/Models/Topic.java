package com.example.maria.androidacademyspb.Models;

public class Topic {

    private int topicId;
    private String topicDate;
    private String topicTime;
    private String topicTitle;
    private String topicDescription;
    private String topicAuthor;
    private String topicLabel;
    private String topicRoom;

    public Topic(int topicId, String topicDate, String topicTime, String topicTitle, String topicDescription, String topicAuthor, String topicLabel, String topicRoom) {
        this.topicId = topicId;
        this.topicDate = topicDate;
        this.topicTime = topicTime;
        this.topicTitle = topicTitle;
        this.topicDescription = topicDescription;
        this.topicAuthor = topicAuthor;
        this.topicLabel = topicLabel;
        this.topicRoom = topicRoom;
    }

    public Topic(int topicId, String topicDate, String topicTime, String topicTitle, String topicAuthor) {
        this.topicId = topicId;
        this.topicDate = topicDate;
        this.topicTime = topicTime;
        this.topicTitle = topicTitle;
        this.topicAuthor = topicAuthor;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }

    public String getTopicDate() {
        return topicDate;
    }

    public void setTopicDate(String topicDate) {
        this.topicDate = topicDate;
    }

    public String getTopicTime() {
        return topicTime;
    }

    public void setTopicTime(String topicTime) {
        this.topicTime = topicTime;
    }

    public String getTopicTitle() {
        return topicTitle;
    }

    public void setTopicTitle(String topicTitle) {
        this.topicTitle = topicTitle;
    }

    public String getTopicDescription() {
        return topicDescription;
    }

    public void setTopicDescription(String topicDescription) {
        this.topicDescription = topicDescription;
    }

    public String getTopicAuthor() {
        return topicAuthor;
    }

    public void setTopicAuthor(String topicAuthor) {
        this.topicAuthor = topicAuthor;
    }

    public String getTopicLabel() {
        return topicLabel;
    }

    public void setTopicLabel(String topicLabel) {
        this.topicLabel = topicLabel;
    }

    public String getTopicRoom() {
        return topicRoom;
    }

    public void setTopicRoom(String topicRoom) {
        this.topicRoom = topicRoom;
    }
}
