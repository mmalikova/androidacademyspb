package com.example.maria.androidacademyspb.Models;

public class Speaker {

    private String speakerName;
    private String speakerPosition;
    private String speakerLocation;
    private String speakerDescription;

    public Speaker(String speakerName, String speakerPosition, String speakerLocation, String speakerDescription) {
        this.speakerName = speakerName;
        this.speakerPosition = speakerPosition;
        this.speakerLocation = speakerLocation;
        this.speakerDescription = speakerDescription;
    }

    public String getSpeakerName() {
        return speakerName;
    }

    public void setSpeakerName(String speakerName) {
        this.speakerName = speakerName;
    }

    public String getSpeakerPosition() {
        return speakerPosition;
    }

    public void setSpeakerPosition(String speakerPosition) {
        this.speakerPosition = speakerPosition;
    }

    public String getSpeakerLocation() {
        return speakerLocation;
    }

    public void setSpeakerLocation(String speakerLocation) {
        this.speakerLocation = speakerLocation;
    }

    public String getSpeakerDescription() {
        return speakerDescription;
    }

    public void setSpeakerDescription(String speakerDescription) {
        this.speakerDescription = speakerDescription;
    }
}
