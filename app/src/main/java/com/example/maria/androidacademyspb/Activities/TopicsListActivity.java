package com.example.maria.androidacademyspb.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.example.maria.androidacademyspb.Adapters.TopicsListAdapter;
import com.example.maria.androidacademyspb.Models.Topic;
import com.example.maria.androidacademyspb.R;

import java.util.ArrayList;

public class TopicsListActivity extends AppCompatActivity implements TopicsListAdapter.ItemClickListener{

    ArrayList<Topic> topics = new ArrayList<Topic>();
    TopicsListAdapter topicsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topics_list);

        // создаем адаптер
        fillData();
        topicsAdapter = new TopicsListAdapter(this, topics);
        topicsAdapter.setClickListener(this);

        // настраиваем список
        RecyclerView listView = findViewById(R.id.topicsList);
        listView.setLayoutManager(new LinearLayoutManager(this));
        listView.setAdapter(topicsAdapter);
    }

    // генерируем данные для адаптера
    void fillData() {
        for (int i = 1; i <= 20; i++) {
            topics.add(new Topic(i, "21 апреля", "15:30", "Доклад" + i, "Иван Иванов"));
        }
    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked " + topicsAdapter.getItemId(position) + " on row number " + position, Toast.LENGTH_SHORT).show();
        Intent topicActivityIntent = new Intent (this, TopicActivity.class);
        startActivity(topicActivityIntent);
    }
}
