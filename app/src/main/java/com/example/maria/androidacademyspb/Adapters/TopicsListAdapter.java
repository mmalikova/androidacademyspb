package com.example.maria.androidacademyspb.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.maria.androidacademyspb.Models.Topic;
import com.example.maria.androidacademyspb.R;

import java.util.ArrayList;

public class TopicsListAdapter extends RecyclerView.Adapter<TopicsListAdapter.ViewHolder> {

    private ArrayList<Topic> topicsList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public TopicsListAdapter(Context context, ArrayList<Topic> data) {
        this.mInflater = LayoutInflater.from(context);
        this.topicsList = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_topic_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Topic topicItem = topicsList.get(position);
        holder.itemTopicTitle.setText(topicItem.getTopicTitle());
        holder.itemTopicSpeaker.setText(topicItem.getTopicAuthor());
        holder.itemTopicTime.setText(topicItem.getTopicTime());
        holder.itemTopicDate.setText(topicItem.getTopicDate());

    }

    // total number of rows
    @Override
    public int getItemCount() {
        return topicsList.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView itemTopicTitle;
        TextView itemTopicSpeaker;
        TextView itemTopicTime;
        TextView itemTopicDate;

        ViewHolder(View itemView) {
            super(itemView);
            itemTopicTitle = itemView.findViewById(R.id.itemTopicTitle);
            itemTopicSpeaker = itemView.findViewById(R.id.itemTopicSpeaker);
            itemTopicTime = itemView.findViewById(R.id.itemTopicTime);
            itemTopicDate = itemView.findViewById(R.id.itemTopicDate);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Topic getItem(int id) {
        return topicsList.get(id);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

/*public class TopicsListAdapter extends BaseAdapter {

    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Topic> objects;

    public TopicsListAdapter(Context context, ArrayList<Topic> topics) {
        ctx = context;
        objects = topics;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return objects.get(position).getTopicId();
    }

    Topic getTopic(int position) {
        return ((Topic) getItem(position));
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item_topic_list, parent, false);
        }

        Topic topic = getTopic(position);

        ((TextView) view.findViewById(R.id.itemTopicTitle)).setText(topic.getTopicTitle());
        ((TextView) view.findViewById(R.id.itemTopicSpeaker)).setText(topic.getTopicAuthor());
        ((TextView) view.findViewById(R.id.itemTopicTime)).setText(topic.getTopicTitle());
        ((TextView) view.findViewById(R.id.itemTopicDate)).setText(topic.getTopicDate());

        return view;
    }
}
*/
