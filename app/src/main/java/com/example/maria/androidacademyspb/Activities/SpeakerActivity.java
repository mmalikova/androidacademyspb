package com.example.maria.androidacademyspb.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.TextView;

import com.example.maria.androidacademyspb.Models.Speaker;
import com.example.maria.androidacademyspb.R;

public class SpeakerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_speaker);

        Speaker speaker = new Speaker("IVANOV \nIVAN", "QA Engeneer", "Berlin, Germany", "Tanay Pant is an author, hacker, developer and tech enthusiast. He is best known for his work on “Learning Web-based Virtual Reality” by Apress, “Building a Virtual Assistant for Raspberry Pi” by Apress and “Learning Firefox OS Application Development” published by Packt. He is also an official representative of Mozilla. He has been listed in the about:credits of the Firefox web browser for his contributions to the different open source projects of the Mozilla Foundation. He also writes for a number of websites like SitePoint and Tuts+ where he shares tips and tricks about web development.");

        TextView speakerName = findViewById(R.id.speakerName);
        speakerName.setText(speaker.getSpeakerName());

        TextView speakerPosition = findViewById(R.id.speakerPosition);
        speakerPosition.setText(speaker.getSpeakerPosition());

        TextView speakerLocation = findViewById(R.id.speakerLocation);
        speakerLocation.setText(speaker.getSpeakerLocation());

        TextView speakerDescription = findViewById(R.id.speakerDesctiption);
        speakerDescription.setText(speaker.getSpeakerDescription());
    }
}
