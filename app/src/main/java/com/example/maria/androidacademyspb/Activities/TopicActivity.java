package com.example.maria.androidacademyspb.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.maria.androidacademyspb.Models.Topic;
import com.example.maria.androidacademyspb.R;

public class TopicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        Topic topic = new Topic(1,"15 марта", "15:30", "Как мы ускоряем Яндекс под Android",
                "Lorem Ipsum - это текст-'рыба', часто используемый в печати и вэб-дизайне. Lorem Ipsum является стандартной 'рыбой' для текстов на латинице с начала XVI века. В то время некий безымянный печатник создал большую коллекцию размеров и форм шрифтов, используя Lorem Ipsum для распечатки образцов. Lorem Ipsum не только успешно пережил без заметных изменений пять веков, но и перешагнул в электронный дизайн. Его популяризации в новое время послужили публикация листов Letraset с образцами Lorem Ipsum в 60-х годах и, в более недавнее время, программы электронной вёрстки типа Aldus PageMaker, в шаблонах которых используется Lorem Ipsum.",
                "Артур Василов", "Android", "404");

        TextView reportDate = findViewById(R.id.topicDate);
        reportDate.setText(topic.getTopicDate());

        TextView reportTime = findViewById(R.id.topicTime);
        reportTime.setText(topic.getTopicTime());

        TextView reportTitle = findViewById(R.id.topicTitle);
        reportTitle.setText(topic.getTopicTitle());

        TextView reportDescription = findViewById(R.id.topicDescription);
        reportDescription.setText(topic.getTopicDescription());

        TextView reportAuthor = findViewById(R.id.topicAuthor);
        reportAuthor.setText(topic.getTopicAuthor());
        reportAuthor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openReporterInfo();
            }
        });

        Button reportLabel = findViewById(R.id.topicLabel);
        reportLabel.setText(topic.getTopicLabel());

        TextView reportRoom = findViewById(R.id.topicRoom);
        reportRoom.setText(topic.getTopicRoom());

        Button allReportsButton = findViewById(R.id.allReports);
        allReportsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity();
            }
        });

    }

    private void openActivity() {
        Intent secondActivityIntent = new Intent(this, MainActivity.class);
        startActivity(secondActivityIntent);
    }

    private void openReporterInfo(){
        Intent reporterActivityIntent = new Intent (this, TopicActivity.class);
        startActivity(reporterActivityIntent);
    }
}
